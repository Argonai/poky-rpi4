#!/bin/sh
#if ! [ $(id -u) = 0 ]; then
#   echo "This script must be run as root"
#   exit 1
#fi
cp create_sdcard_images.sh rpi64/meta-rpi64/scripts
DISK_NAME="$(lsblk -o NAME,RM,TYPE | grep 1 | grep disk | cut -c1-3)"
echo "***Unmounting disk***"
umount /dev/$DISK_NAME?*
. ./rpi64/meta-rpi64/scripts/mk2parts.sh $DISK_NAME
if [ -d /media/card ]; then
  echo "mountpoint already exists\n"
else
  echo "***Making mountpoint***\n"
  sudo mkdir /media/card
fi
if [ -z "$OETMP" ]; then
    # echo try to find it
    if [ -f rpi64/build/conf/local.conf ]; then
        export OETMP=$(grep '^TMPDIR' rpi64/build/conf/local.conf | awk '{ print $3 }' | sed 's/"//g')
    fi

    if [ -z "$OETMP" ]; then
        if [ -d "rpi64/build/tmp" ]; then
            export OETMP=rpi64/build/tmp
        fi
    fi
fi
. compile.sh
cd ../rpi64/meta-rpi64/scripts/
./create_sdcard_images.sh
