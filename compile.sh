#! /bin/sh
#a bash script to create the build environment, install the config files and
#compile the image
#||| checks if the script is run in its own directory
#VVV
if [ -n "$BASH_SOURCE" ]; then
    THIS_SCRIPT=$BASH_SOURCE
elif [ -n "$ZSH_NAME" ]; then
    THIS_SCRIPT=$0
else
    THIS_SCRIPT="$(pwd)/compile.sh"
    if [ ! -e "$THIS_SCRIPT" ]; then
        echo "Error: $THIS_SCRIPT doesn't exist!" >&2
        echo "Please run this script in compile.sh's directory." >&2
        exit 1
    fi
fi
#||| checks ig the scipt is sourced
#VVV
if [ -z "$ZSH_NAME" ] && [ "$0" = "$THIS_SCRIPT" ]; then
    echo "Error: This script needs to be sourced. Please run as '. $THIS_SCRIPT'" >&2
    exit 1
fi
#|||Compiles the image
#VVV
BASE_DIRECTORY="$(pwd)"
source $PWD/poky/oe-init-build-env $PWD/rpi64/build
cd $BASE_DIRECTORY
scp $PWD/conf/local.conf $PWD/rpi64/build/conf/local.conf
scp $PWD/conf/bblayers.conf $PWD/rpi64/build/conf/bblayers.conf
rm $PWD/rpi64/build/conf/templateconf.cfg

cp $PWD/conf/console-image.bb $PWD/rpi64/meta-rpi64/images/console-image.bb
cd rpi64/build
bitbake console-image
