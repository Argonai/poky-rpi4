rsyslog DONE, rsyslog zit in meta-openembedded/meta-oue/recipes-extended
logrotate DONE, logrotate zit in poky/meta/recipes-extended
nmcli DONE, zit normaalgezien bij networkmanager
mosquitto(broker) DONE
https://docs.armbian.com/#performance-tweaks
voor elke 10 minuten te loggen moet de ext4 config veranderd worden (commit=600) https://www.kernel.org/doc/Documentation/filesystems/ext4.txt
log2ram, het is waarschijnlijk mogelijk om een recipe te maken gebaseerd op het installscript van log2ram zelf https://github.com/azlux/log2ram/blob/master/install.sh
aangezien beide de install command gebruiken

van source te builden: ansible DONE, momenteel versie 2.9.9 https://releases.ansible.com/ansible/ aangezien ansible nu PyPi gebruikt voor releases https://pypi.org/project/ansible/
op ssh toegang de status van de pi tonen (RAM, opslag, CPU, etc...) https://superuser.com/questions/355029/how-to-automatically-run-commands-on-ssh-login
systemd scripts https://stackoverflow.com/questions/45614578/enable-systemd-services-using-yocto
SSH keys en andere custom config files https://yocto.yoctoproject.narkive.com/XAP8ZleQ/a-simpler-way-to-apply-custom-config-files-to-an-image

DONE: enkel eth, serial, USB, APM, efi, pcbios, pci, enkel screen tijdens development
usbhost
distro features: ipv6, ldconfig, pci, systemd, usbhost, usrmerge
image features: package management, post-install logging, read only rootfs, ssh server openssh



TODO: 
	1) Ansible toevoegen aan layer (voegt ongeveer 100 packages toe), ansible zit nu in yocto maar
	het is ansible 2.3 wat niet compatible is met python 3.7 en hoger, moet kijken hoe ik een hogere versie kan installeren
	hogere versie geinstalleerd, moet nu nog bij installscript toevoegen dat het de nieuwste versie installeerd
	ANSIBLE IS GPLv3
	2) nmcli https://layers.openembedded.org/layerindex/branch/master/layer/meta-networking/
	3) script om alle software met apt te installeren, misschien  niet nodig, met do_install_append() misschien in een bbappend bestand met de install command https://linux.die.net/man/1/install
	
	
	