# Installation guide
- [Table of content](#installation-guide)
- [Prerequisites](#prerequisites)
    - [OS specifications](#os-specifications)
    - [Required packages](#required-packages)
    - [Repository](#copy-the-poky-rpi4-repository-with-git)
- [Configuration](#configuration)
    - [bblayers.conf](#editing-bblayers.conf)
    - [local.conf](#editing-local.vonf)
      - [Debug tweaks](#debug-tweaks)
      - [TMPDIR](#tmpdir)
      - [DL_DIR](#dl_dir)
    - [Compiling](#running-the-compile.sh-script)
- [Installation](#installation)
  - [Formatting](#formatting-the-SD-kaart)
  - [Mount point](#make-a-temporary-mointpoint)
  - [Copying image](#copying-the-image)
    - [Installing bootloader](#copy_boot.sh)
    - [Installing root filesystem](#copy_rootfs.sh)


# Prerequisites

## OS specifications
 * 70-90 Gigabyte of free space
 * A linux distro which can run the required software, see the docs for [officially supported distros](https://docs.yoctoproject.org/ref-manual/system-requirements.html#supported-linux-distributions)
 * Git 1.8.3.1 or higher
 * tar 1.28 or higher
 * Python 3.6 or higher
 * gcc 5.0 or higher

## Required packages

The following packages are required to compile an image, use this command to install them:
```
$ sudo apt-get install gawk wget git diffstat unzip texinfo gcc build-essential chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm python3-subunit mesa-common-dev
```
## Copy the poky-rpi4 repository with git
This is a repository with all layers as submodules and a few scripts to help compiling an installing the image to an SD card.

```
~$ git clone --recurse-submodules
```
The `--recurse-submodules` is required to clone the submodules, you will have to manually install the repositories yourself if you don't use it. See the `.gitmodules` file for where you need to clone the repository

# Configuration
There are 2 files that you may want to edit, you will find the files in the **conf** folder

### Editing bblayers.conf

If you installed a submodule in a different location then defined in the **bblayers.conf** you will have to edit the file and change the path.

**WARNING**: You cannot use '**~**' in the Yocto configuration files, you should use `${HOME}` instead.

### Editing local.conf

There are a few features that you might want to change, you can change them in the **local.conf** file

#### Debug-tweaks
  The default root password will be empty, you can enable this by uncommenting this line:
  ```
  #EXTRA_IMAGE_FEATURES = "debug-tweaks"
  ```
  and commenting these four lines
  ```
  INHERIT += "extrausers"
  EXTRA_USERS_PARAMS = "usermod -P root root; "

  INHERIT += "chageusers"
  CHAGE_USERS_PARAMS = "chage -d0 root; "
  ```
#### TMPDIR

This is the directory where your image will be when it's finished, by default it is **~/poky-rpi4/rpi64/build/sources**. This folder will be around 20-30 Gigabytes.

If you used a different directory, make sure that the current user can write in that directory.

#### DL_DIR
This is where everything that downloads during compiling will end up, this folder can be shared between projects so it's interesting to change this if you're making multiple images.

The default directory is: **~/poky-rpi4/rpi64/build/sources**

#### MACHINE_FEATURES_remove

These are features you can disable, see the [official docs](https://docs.yoctoproject.org/ref-manual/features.html?highlight=extra_image_features#machine-features) for more info

#### EXTRA_IMAGE_FEATURES

These are features you can enable, see the [official docs](https://docs.yoctoproject.org/ref-manual/features.html?highlight=extra_image_features#image-features)

## Run the compile.sh script

To start the automatic installation, run the `compile.sh` script. Be sure that you're working in the **poky-rpi4** directory.

```
~/poky-rpi4$ source ./compile.sh
```
This script is easily modifiable
# Installation
Now that the image is compiled you'll have to put it on an SD card.

## Automatic

### Create an image
The meta-rpi64 repository contains a script for creating an image.

#### Setting environment variables
The script needs 2 environment variables, where the compiled image is located (The TMPDIR variable you set in local.conf) and which machine the image will run on. Use the following 2 commands to set them.
```
$ export MACHINE=raspberrypi4-64
```
If you didn't set the TMPDIR in local.conf use the default location, else replace it in the following command.
```
$ export OETMP=~/poky-rpi4/rpi64/build/tmp
```
#### Creating the images
Use the following commands to navigate to the folder where the script is located and execute it.

```
$ cd ~/poky-rpi4/rpi64/meta-rpi64/scripts
~/poky-rpi4/rpi64/meta-rpi64/scripts$ ./create_sdcard_images.sh
```
#### Putting the image on an SD-card
The image will be located in **~/poky-rpi4/rpi64/upload**, you can then use software like rufus or balena-etcher to put it on an SD-card

## Manual

## Formatting the SD-kaart
Use the command `lsblk`, your output should look like this:
```
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  55,5M  1 loop /snap/core18/1988
loop1    7:1    0  55,5M  1 loop /snap/core18/1997
loop2    7:2    0   219M  1 loop /snap/gnome-3-34-1804/66
loop3    7:3    0  64,8M  1 loop /snap/gtk-common-themes/1514
loop4    7:4    0  31,1M  1 loop /snap/snapd/11036
loop5    7:5    0    51M  1 loop /snap/snap-store/518
loop6    7:6    0  32,3M  1 loop /snap/snapd/11588
sda      8:0    0 141,7G  0 disk
├─sda1   8:1    0   512M  0 part /boot/efi
├─sda2   8:2    0     1K  0 part
└─sda5   8:5    0 141,2G  0 part /
sdb      8:16   1  14,9G  0 disk
├─sdb1   8:17   1    64M  0 part /media/aaron/BOOT
└─sdb2   8:18   1  14,8G  0 part /media/aaron/ROOT
```
You can see my SD card is labeled **sdb** and has 2 partitions, unmount unmount all partitions on your card with the following.

Don't forget to replace `{MOUNTPOINT}` with the location you got from `lsblk` . If you have multiple mountpoints you will need to run this command multiple times
```
~$ sudo umount {MOUNTPOINT}
```


You will need to run the following command to partition you SD card, don't forget to replace `{NAME}` with the name of the partition you got from `lsblk` (in my case **sdb**)
```
~$ cd ~/rpi64/meta-rpi64/scripts
~/rpi64/meta-rpi64/scripts$ sudo ./mk2parts.sh {NAME}
```
## Make a temporary mointpoint

You will need to make a temporary mountpoint for the next script, do this with the following command
```
~$ sudo mkdir /media/card
```
## Copying the image

We will use 2 scripts for this step, one for the bootloader and one for the filesystem.

### copy_boot.sh

This is the script for the bootloader. The script will have to know what you used as the TMPDIR, it does this by searching for an environment variable called **OETMP**.

Execute the following commands to install the bootloader. Don't forget to replace `{TMPDIR}` with what you used ad **TMPDIR**(If you didn't change **TMPDIR** it will be `~/poky-rpi/rpi64/build/tmp`) and `{NAME}` with the name of the SD card from lsblk `lsblk`
```
~$ cd ~/poky-rpi4/rpi64/meta-rpi64/scripts
~/poky-rpi4/rpi64/meta-rpi64/scripts$ export OETMP={TMPDIR}
~/poky-rpi4/rpi64/meta-rpi64/scripts$ ./copy_boot.sh {NAME}
```
### copy_rootfs.sh

This script will copy the root filesystem to the SD-card. Again, don't forget to change `{NAME}` with the name of the SD-card from `lsblk`
```
/poky-rpi4/rpi64/meta-rpi64/scripts$ ./copy_rootfs.sh {NAME} console
```
